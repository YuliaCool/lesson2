import {FighterService} from './services/fightersService';

class Fighter{
    name;
    health;
    attack;
    defense;

    /*constructor(name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack= attack;
        this.defense = defense;
    }*/
    constructor(candidate) {
        this.name = candidate.name;
        this.health = candidate.health;
        this.attack= candidate.attack;
        this.defense = candidate.defense;
    }

    getHitPower(){
        let criticalHitChance = getRandomInInterval1to2();
        return this.attack * criticalHitChance;
    }

    getBlockPower(){
        let dodgeChance = getRandomInInterval1to2();
        return this.defence * dodgeChance;
    }

    getRandomInInterval1to2(){
        return (Math.random().toFixed(2) + 1);
    }

    setDamage(hit){
        let damage = hit - this.getBlockPower();

        if (damage <= 0) return; 

        if(this.health > hit){
            this.health -= hit;
        }
        else {
            this.health = 0;
            alert ("Fighter " + this.name + " has been defeated :(");
        }
    }
}
export default Fighter;