import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  
  async getFighterDetails(_id) {
    // implement this method
    // endpoint - `details/fighter/${_id}.json`;
    try {
      console.log(_id);

      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation)
      {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      allInformation = JSON.parse(atob(apiResult.content));
      }
      function* getNextProperty(){
        const result = yield "ID: "+allInformation._id+'\n';
        yield "name: "+ allInformation.name+'\n';
        yield "attack: "+allInformation.attack+'\n';
        yield "defense: "+allInformation.defense+'\n';
        yield "health: "+allInformation.health+'\n';
      }

      let parsedInfoLine = getNextProperty();
      let idValue = parsedInfoLine.next().value;
      let nameValue = parsedInfoLine.next().value;
      let attackValue = parsedInfoLine.next().value;
      let defenseValue = parsedInfoLine.next().value;
      let healthValue = parsedInfoLine.next().value;
      
      //return JSON.parse(atob(apiResult.content));
      return idValue + nameValue + attackValue + defenseValue + healthValue;
    } catch (error){
      throw error;
    }
    
  }

  async getFightersName(_id){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      allInformation = JSON.parse(atob(apiResult.content));
      }
      return allInformation.name;
    }
    catch(error){
      throw error;
    }
  }
  async getFightersDefense(_id){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      allInformation = JSON.parse(atob(apiResult.content));
      }
      return allInformation.defense;
    }
    catch(error){
      throw error;
    }
  }

  async getFightersHealth(_id){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      allInformation = JSON.parse(atob(apiResult.content));
      }
      return allInformation.health;
    }
    catch(error){
      throw error;
    }
  }

  async updateAttack(_id, newValue){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
        const endpoint = `details/fighter/${_id}.json`;
        const apiResult = await callApi(endpoint, 'GET');
        allInformation = JSON.parse(atob(apiResult.content));
      }

      console.log("from updateAttack (before) = "+allInformation.attack);
      allInformation.attack = newValue;
      localStorage.setItem(`${_id}`, JSON.stringify(allInformation));

      console.log("from updateAttack (after) = "+allInformation.attack);
    
      /*fs = require('fs');
      var m = JSON.parse(fs.readFileSync(`details/fighter/${_id}.json`).toString());
      m.attack = newValue;
      fs.writeFile(`details/fighter/${_id}.json`, JSON.stringify(m));*/
    }
    catch (error){
      throw error;
    }
  }

  async updateDefense(_id, newValue){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
        const endpoint = `details/fighter/${_id}.json`;
        const apiResult = await callApi(endpoint, 'GET');
        allInformation = JSON.parse(atob(apiResult.content));
      }

      console.log("from updateDefense (before) = "+allInformation.defense);
      allInformation.defense = newValue;
      localStorage.setItem(`${_id}`, JSON.stringify(allInformation));
      console.log("from updateDefense (after)= "+allInformation.defense);
    }
    catch (error){
      throw error;
    }
  }

  async updateHealth(_id, newValue){
    try{
      let allInformation = JSON.parse(localStorage.getItem(`${_id}`));
      if(!allInformation){
        const endpoint = `details/fighter/${_id}.json`;
        const apiResult = await callApi(endpoint, 'GET');
        allInformation = JSON.parse(atob(apiResult.content));
      }

      console.log("from updateHealth (before)= "+allInformation.health);
      allInformation.health = newValue;
      localStorage.setItem(`${_id}`, JSON.stringify(allInformation));
      console.log("from updateHealth (after) = "+allInformation.health);
    }
    catch (error){
      throw error;
    }
  }

}

export const fighterService = new FighterService();
