import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './Fighter';

var FighterSettingWindow;

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    //this.element = this.createElement
  }

  async handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    console.log('clicked');
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
    const fightersInfo = await fighterService.getFighterDetails(fighter._id);
    console.log(fightersInfo);
    const fighterName = await fighterService.getFightersName(fighter._id);
    const fighterHealth = await fighterService.getFightersHealth(fighter._id);
    const fighterDefense = await fighterService.getFightersDefense(fighter._id);
    //await fighterService.updateAttack(fighter._id, 100);
    //await fighterService.updateHealth(fighter._id, 150);
    //await fighterService.updateDefense(fighter._id, 200);
    
    let changedFields = openSettingsWindow();
    /*
    console.log(changedFields);
    for(let i in changedFields){
      console.log(i);
    }
    for(let i of changedFields){
      console.log(i);
    }*/


    function openSettingsWindow(){
      FighterSettingWindow = window.open('about:blank', "Edit Fighter settings");
      let newDocument = FighterSettingWindow.document;
      newDocument.open();
      newDocument.write('<!DOCTYPE html><html><head><style type="text/css"></style><script type="text/javascript"></script></head><body></body></html>');
      newDocument.close();

      const changesMap = new Map();

      let div1 = newDocument.createElement('div');
      newDocument.body.appendChild(div1);

      let labelName = newDocument.createElement("label");
      labelName.innerHTML = "Fighter's Name ";
      newDocument.body.appendChild(labelName);

      let inputName = newDocument.createElement('input');
      inputName.setAttribute("value", fighterName);
      inputName.readOnly = true;
      inputName.disabled = true;
      newDocument.body.appendChild(inputName);

      let div2 = newDocument.createElement('div');
      newDocument.body.appendChild(div2);

      let labelHealth = newDocument.createElement("label");
      labelHealth.innerHTML = "Fighter's Health ";
      newDocument.body.appendChild(labelHealth);

      let inputHealth = newDocument.createElement('input');
      inputHealth.setAttribute("value", fighterHealth);
      inputHealth.id = "HealthId";
      newDocument.body.appendChild(inputHealth);

      let div3 = newDocument.createElement('div');
      newDocument.body.appendChild(div3);

      let labelDefense = newDocument.createElement("label");
      labelDefense.innerHTML = "Fighter's Defense ";
      newDocument.body.appendChild(labelDefense);

      let inputDefense = newDocument.createElement('input');
      inputDefense.setAttribute("value", fighterDefense);
      inputDefense.id = "DefenseId";
      newDocument.body.appendChild(inputDefense);

      let div5 = newDocument.createElement('div');
      newDocument.body.appendChild(div5);

      let labelAttack = newDocument.createElement("label");
      labelAttack.innerHTML = "Fighter's Attack ";
      newDocument.body.appendChild(labelAttack);

      let inputAttack = newDocument.createElement('input');
      inputAttack.setAttribute("value", fighterDefense);
      inputAttack.id = "AttackId";
      newDocument.body.appendChild(inputAttack);

      let div6 = newDocument.createElement('div');
      newDocument.body.appendChild(div6);
      
      let buttonSave = newDocument.createElement("BUTTON");
      buttonSave.innerText = "Update values";   
      buttonSave.onclick = function(){
       let newHealth = newDocument.getElementById("HealthId").value;
       let newDefense = newDocument.getElementById("DefenseId").value;
       let newAttack = newDocument.getElementById("AttackId").value;
      // if(new String(newHealth).valueOf() != new String(fighterHealth).valueOf()){ - additional variant
        if(inputHealth.defaultValue != inputHealth.value){
         console.log("Health has been changed!");
         fighterService.updateHealth(fighter._id, newHealth);
         //changesMap.set('health', true);
       }
       if(inputAttack.defaultValue != inputAttack.value){
        console.log("Attack has been changed!");
        fighterService.updateAttack(fighter._id, newAttack);
        //changesMap.set('attack', true);
      }
       if(inputDefense.defaultValue != inputDefense.value){
        console.log("Defense has been changed!");
        fighterService.updateDefense(fighter._id, newDefense);
        //changesMap.set('defence', true);
      }

       FighterSettingWindow.close();
      };     
      newDocument.body.appendChild(buttonSave);

      let buttonClose = newDocument.createElement("BUTTON");
      buttonClose.innerText = "Close settings";   
      buttonClose.onclick = function(){
        FighterSettingWindow.close();
      }

      newDocument.body.appendChild(buttonClose);

      //return changesMap;
      }
  }

  
  fight(fighter1, fighter2){
    let blueTrousers = new Fighter(fighter1);
    let redTrousers = new Fighter(fighter2);

    blueTrousers.setDamage(redTrousers.getHitPower());
    redTrousers.setDamage(blueTrousers.getHitPower());
  }

}

export default FightersView;